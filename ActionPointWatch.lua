--[[

    Watch Ability Point regeneration.


--]]

ActionPointWatch = {}

local function print(txt)
    TextLogAddEntry("Chat", 0, towstring(txt))
end

function ActionPointWatch.Initialize()
    -- savedvariables
    if not ActionPointOptions then
        ActionPointOptions = {
            previousAP = 0,
            isWCTDisabled = false,
            WSCTFrame = 2,
            useBoth = false,
            AP_Version = "1.0",
        }
    end

    RegisterEventHandler(SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "ActionPointWatch.UpdateCurrentActionPoints")
    
   if not LibSlash.IsSlashCmdRegistered("/apw") then
       LibSlash.RegisterWSlashCmd("apw", function(args) ActionPointWatch.SlashHandler(args) end)
       LibSlash.RegisterWSlashCmd("/actionpointwatch", function(args) ActionPointWatch.SlashHandler(args) end)
    else      
       print("Warning: something else seems to be using /apw - ActionPointWatch won't be able to. Use /actionpointwatch instead.")
       LibSlash.RegisterWSlashCmd("actionpointwatch", function(args) ActionPointWatch.SlashHandler(args) end)
    end
end

-- shutdown
function ActionPointWatch.OnShutdown()
    UnRegisterEventHandler(SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "ActionPointWatch.UpdateCurrentActionPoints")
end

function ActionPointWatch.UpdateCurrentActionPoints()
    local diffAP = 0
    local prevAP = ActionPointOptions.previousAP
    local curAP = GameData.Player.actionPoints.current
    local diffChar = "="
    if (prevAP <= 0) then
        diffAP = 0
        diffChar = ""
    elseif (prevAP > curAP) then
        diffAP = curAP - prevAP
        diffChar = ""
    elseif (prevAP < curAP) then
        diffAP = curAP - prevAP
        diffChar = "+"
    end

    ActionPointOptions.previousAP = GameData.Player.actionPoints.current
    
    -- console output if they use it
    if (ActionPointOptions.useBoth or ActionPointOptions.isWCTDisabled) then
        TextLogAddEntry("Chat", 0,L"-APW:"..GameData.Player.actionPoints.current
                            ..L"/"..towstring(diffChar)..L""..towstring(diffAP))
    end
    
    -- display via WSCT
    local updateAP = towstring(diffChar)..L""..towstring(diffAP)
    if (not ActionPointOptions.isWCTDisabled or ActionPointOptions.useBoth) then
        ActionPointWatch.WSCTPrint(updateAP)
    end
end



-- WSCT print, ability is ready.
function ActionPointWatch.WSCTPrint(updateAP)
    local colorWSCT = {r = 255, g = 255, b = 0} -- yellow
    local isCritWSCT = false
    local etypeWSCT = nil
    local frameWSCT = 2
    local anitypeWSCT = nil
    local objectIDWSCT = nil
    local iconWSCT = nil
    local msgWSCT = updateAP
    
--    if WSCT then    
        if ActionPointOptions.WSCTFrame == 1 then
            frameWSCT = WSCT.FRAME1
        elseif ActionPointOptions.WSCTFrame == 2 then
            frameWSCT = WSCT.FRAME2
        elseif ActionPointOptions.WSCTFrame == 3 then
            frameWSCT = WSCT.FRAME3
        else
            frameWSCT = WSCT.FRAME3
        end    
--    end

--WSCT.DisplayText(msg, color, iscrit, etype, frame, anitype, objectID, icon)
--msg - the string to display (already converted to wstring)
--color- the color table, ex. {r=0,g=0,b=255}
--iscrit - boolean for if it is a crit or not
--etype - should be nil
--frame - which WSCT frame (1,2,3)
--anitype - which animation type to use (1-8, or if nil will use the frame's default)
--objectID - the world obj ID if trying to attach, can be nil
--icon - should be nil, icons aren't supported yet
 WSCT:DisplayText(msgWSCT,
                 colorWSCT,
                 isCritWSCT,
                 etypeWSCT,
                 frameWSCT,
                 anitypeWSCT,
                 objectIDWSCT,
                 iconWSCT)
end


-- list slash commands
function ActionPointWatch.CommandOptions()
        TextLogAddEntry("Chat", 0, L"------------ActionPointWatch! "..towstring(ActionPointOptions.AP_Version)..L"-----------------")
        print("Valid options for /apw:")
        print("wsct [enable|disable|both] - enable/disable WSCT output or use both standard/WSCT")
        print("wsctframe [1|2|3] - change WSCT output frame to 1, 2 or 3.")

end
-- process slash commands
function ActionPointWatch.SlashHandler(args)
    local opt, val = args:match(L"([a-z0-9]+)[ ]?(.*)")
    
    -- NO OPTION SPECIFIED
    if not opt then
        ActionPointWatch.CommandOptions()        
        
    -- enable/disable WSCT support
    elseif opt == L"wsct" then
        if val == L"" or not val then
            print("Usage: /actionpointwatch wsct [enable|disable]")
        elseif (val:lower()) == L"disable" then
            ActionPointOptions.useBoth = false
            ActionPointOptions.isWCTDisabled = true
            print("WSCT output disabled.")
        elseif (val:lower()) == L"enable" then
            ActionPointOptions.useBoth = false
            ActionPointOptions.isWCTDisabled = false
            if WSCT then
                print("WSCT output enabled.")
            else
                print("WSCT output enabled but you do not have WSCT installed so will not function.")
            end
        elseif (val:lower()) == L"both" then
            ActionPointOptions.useBoth = true
            if WSCT then
                print("Standard and WSCT output both enabled.")
            else
                print("Standard and WSCT output both enabled but you do not have WSCT installed.")
            end
        else
            print("Usage: /actionpointwatch wsct [enable|disable]")
        end
    -- set WSCT frame
    elseif opt == L"wsctframe" then
        if val == L"" or not val then
            print("Usage: /actionpointwatch wsctframe [1|2|3]")
        elseif (val:lower()) == L"1" then
            ActionPointOptions.WSCTFrame = 1
            print("-WSCT frame set to 1.")
        elseif (val:lower()) == L"2" then
            ActionPointOptions.WSCTFrame = 2
            print("-WSCT frame set to 2.")
        elseif (val:lower()) == L"3" then
            ActionPointOptions.WSCTFrame = 3
            print("-WSCT frame set to 3.")
        else
            print("Usage: /actionpointwatch wsctframe [1|2|3]")
        end

    -- OPTION NOT FOUND
    else
        print(L"'"..opt..L"' is not a valid option for /actionpointwatch.")
        print(L"See '/actionpointwatch' for a list of options.")
    end 
end
