<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="ActionPointWatch" version="1.0" date="11/21/2008" >

		<Author name="Celestian" email="uce_mike@yahoo.com" />
		<Description text="Creates a movable frame which displays your current/diff AP." />

<VersionSettings gameVersion="1.4.5" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<SavedVariables>
		  <SavedVariable name="ActionPointOptions" />
		</SavedVariables>

		<Files>
			<File name="ActionPointWatch.lua" />
		</Files>
		
		<OnInitialize>
		  <CallFunction name="ActionPointWatch.Initialize" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
		  <CallFunction name="ActionPointWatch.OnShutdown" />
		</OnShutdown>
		
	</UiMod>
</ModuleFile>

